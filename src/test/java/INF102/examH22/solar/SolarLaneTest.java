package INF102.examH22.solar;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 * @author Martin Vatshelle
 */
class SolarLaneTest {

	ISolarLane lane;
	
	@BeforeEach
	void init(){
		lane = new SolarLane();	
	}
	
	@Test
	void place1robot() {
		List<Integer> input = of(7,8,13,15);
		Integer pos = lane.place1Robot(input);
		assertEquals(13, computeSum(input, pos));
	}
	@Test
	void place2robots() {
		List<Integer> input = of(7,8,13,15);
		Pair<Integer> pos = lane.place2Robots(input);
		assertEquals(3, computeSum(input, pos));
	}

	@Test
	void place2robotsMultipleSame() {
		List<Integer> input = of(7,8,14,15,15,15,16);
		Pair<Integer> pos = lane.place2Robots(input);
		assertEquals(3, computeSum(input, pos));
	}

	private static List<Integer> of(int ...ints) {
		List<Integer> list = new ArrayList<>();
		for(int i : ints)
			list.add(i);
		return list;
	}
	
	public static int computeSum(List<Integer> list, Integer robot) {
		int sum=0;
		for(Integer i : list) {
			sum += distToRobot(i, robot);
		}
		return sum;
	}
	
	public static int computeSum(List<Integer> list, Pair<Integer> robots) {
		int sum=0;
		for(Integer i : list) {
			sum += distToRobot(i, robots);
		}
		return sum;
	}

	public static int distToRobot(int location, Integer robot) {
		return Math.abs(location-robot);
	}

	public static int distToRobot(int location, Pair<Integer> robots) {
		int d1 = distToRobot(location, robots.loc1);
		int d2 = distToRobot(location, robots.loc2);
		return Math.min(d1,d2);
	}
}
